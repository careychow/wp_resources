<?php
/*
Template Name: Sitemap Page
*/
?>
<?php get_header(); ?>
<div id="content">
	<?php if (have_posts()) : ?><?php while (have_posts()) : the_post(); ?>	
	<!-- menu -->
	<div id="map">
		<div class="browse">现在的位置: <a title="返回首页" href="<?php echo get_settings('Home'); ?>/">首页</a> &gt; <?php the_title(); ?></div>
		<div id="feed"><a href="<?php bloginfo('rss2_url'); ?>" title="RSS">RSS</a></div>
	</div>
	<!-- end: menu -->
	<!-- entry -->
	<div class="clear"></div>
	<div class="entry_box_s">
		<div class="entry">
			<div class="page" id="post-<?php the_ID(); ?>">
				<?php the_content('More &raquo;'); ?>
				<div class="clear"></div>
			</div>
			<?php get_template_part('/partials/sitemap'); ?>
		</div>
		<!-- end: entry -->
		<div class="clear"></div>
		<i class="lt"></i>
		<i class="rt"></i>
	</div>
	<div class="entry_sb">
		<i class="lb"></i>
		<i class="rb"></i>
	</div>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<!-- end: content -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>